"""
blacklist.py

Ce fichier contient juste la liste noire des jetons JWT - il sera importé par
l'application et la ressource de déconnexion afin que les jetons puissent être ajoutés à la liste noire lorsque
l'utilisateur se déconnecte.
"""

BLACKLIST = set()
