# Structure de l'API REST

<center><img src="capture/image.PNG" alt="Alt text (required)" class="image-noshadow"></center>

## app.py

Dans app.py nous allons initialiser et configurer notre application Flask. Nous allons également configurer nos ressources API.

Ce fichier est le point d'entrée de notre API REST


## db.py

Dans ce fichier, nous allons créer notre objet Python de base de données, afin que d'autres fichiers puissent l'importer. Tous les autres fichiers importent la variable de base de données à partir de ce fichier.

La raison de la création d'un fichier séparé contenant uniquement cela est précisément pour qu'il soit plus facile à importer et pour éviter les importations circulaires de Python.

> **NOTE:**
> Dans les applications Python, si de nombreux fichiers importent la même chose, il vaut souvent la peine de le séparer dans un autre fichier.



Voici à quoi ressemble le code pour ce fichier:

```python
from flask_sqlalchemy import SQLAlchemy
​
db = SQLAlchemy()
```

Comme il s'agit d'une extension Flask, elle doit communiquer avec notre application Flask. C'est pourquoi app.py nous avons une ligne qui enregistre cet objet db avec notre app:

```python

...

if __name__ == "__main__":
    db.init_app(app)
    app.run()

```

## Modèles

`models/item.py`

ItemModel contient une définition des données traitées par notre application et des moyens d'interagir avec ces données. Il s'agit essentiellement d'une classe avec quatre propriétés:

> - id;
> - name;
> - price; et
> - store_id.

Les méthodes de la classe peuvent être utilisées pour rechercher des éléments par nom, les enregistrer dans la base de données ou les récupérer. Couvert plus en profondeur ci-dessous.

`models/store.py`

Le StoreModel est une autre définition des données que notre application traite. Il contient deux propriétés:

> - id; et
> - name.

De plus, étant donné que chaque ItemModel possède une propriété `store_id`, StoreModels peut utiliser SQLAlchemy pour trouver ceux qui ont un `store_id` égal à StoreModel `id`. Il peut le faire en utilisant SQLAlchemy `db.relationship()`.

`models/user.py`

Le UserModel est la définition finale des données dans notre API. Ils contiennent:

> - id;
> - username; et
> - password.

## Ressources

`resources/item.py`

Enfin, la ressource est ce qui définit comment les clients interagiront avec notre API REST. Dans la ressource, nous pouvons définir les points de terminaison où les clients devront envoyer des demandes, ainsi que toutes les données qu'ils doivent envoyer dans cette demande.

Par exemple, nous pourrions définir que lorsque les clients envoient une réquête `GET` à `/item/chair`, notre API répondra avec les données d'un élément appelé `chair`. Ces données pourraient être chargées à partir de notre base de données.

> **NOTE :**
> Dans `Flask-RESTful`, nous définissons une `class` pour chaque Resource, et chaque Resource peut avoir une méthode Python pour chaque méthode HTTP à laquelle il devrait pouvoir répondre.
> Par exemple, notre `Itemressource` pourrait définir la façon de répondre à une réquête `GET, POST, DELETE et PUT` de différentes façons.

De plus, `resources/item.py` définit également une ressource `ItemList` qui peut être utilisée pour récupérer plusieurs éléments à la fois via l'API.



`resources/store.py`

De manière similaire à la ressource `Item` , la ressource `Store`  définit la façon dont les utilisateurs interagissent avec notre API.

Les utilisateurs pourront obtenir des magasins, les créer et les supprimer. De même, une ressource `StoreList` est définie pour récupérer tous les magasins de notre API.


`resources/user.py`

Ces ressources sont très différentes des deux autres car elles ne traitent pas seulement de la création et de la mise à jour des données dans notre application, elles traitent également de divers flux d'utilisateurs comme l'authentification, l'actualisation des jetons, les déconnexions, etc.


# Explication En détail: app.py

### Code pourapp.py

```python

from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager

from db import db
from blacklist import BLACKLIST
from resources.user import UserRegister, UserLogin, User, TokenRefresh, UserLogout
from resources.item import Item, ItemList
from resources.store import Store, StoreList

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["PROPAGATE_EXCEPTIONS"] = True
app.config["JWT_BLACKLIST_ENABLED"] = True  # activer la fonction de liste noire
app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = [
    "access",
    "refresh",
]  # autoriser la mise sur liste noire pour l'accès et actualiser les jetons
app.secret_key = "jose"  # pourrait faire app.config ['JWT_SECRET_KEY']
api = Api(app)


@app.before_first_request
def create_tables():
    db.create_all()


jwt = JWTManager(app)


@jwt.user_claims_loader
def add_claims_to_jwt(
    identity
):  # l'identité est ce que nous avons définir lors de la création du jeton d'accès
    if (
        identity == 1
    ):  # au lieu de coder en dur, nous devrions lire à partir d'un fichier ou d'une base de données pour obtenir une liste d'administrateurs à la place
        return {"is_admin": True}
    return {"is_admin": False}


# Cette méthode vérifiera si un jeton est sur liste noire et sera appelée automatiquement lorsque la liste noire est activée
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    return (
        decrypted_token["jti"] in BLACKLIST
    )  # Nous listons ici les JWT particuliers qui ont été créés dans le passé.


# Les rappels suivants sont utilisés pour personnaliser les messages de réponse / d'erreur jwt.
# Les originaux peuvent ne pas être dans un très joli format
@jwt.expired_token_loader
def expired_token_callback():
    return jsonify({"message": "Le jeton a expiré.", "error": "token_expired"}), 401


@jwt.invalid_token_loader
def invalid_token_callback(
    error
):  # nous devons garder l'argument ici, car il est transmis par l'appelant en interne
    return (
        jsonify(
            {"message": "La Vérification de signature a échoué.", "error": "invalid_token"}
        ),
        401,
    )


@jwt.unauthorized_loader
def missing_token_callback(error):
    return (
        jsonify(
            {
                "description": "La demande ne contient pas de jeton d'accès.",
                "error": "authorization_required",
            }
        ),
        401,
    )


@jwt.needs_fresh_token_loader
def token_not_fresh_callback():
    return (
        jsonify(
            {"description": "Le jeton n'est pas active.", "error": "fresh_token_required"}
        ),
        401,
    )


@jwt.revoked_token_loader
def revoked_token_callback():
    return (
        jsonify(
            {"description": "Le jeton a été révoqué.", "error": "token_revoked"}
        ),
        401,
    )


api.add_resource(Store, "/store/<string:name>")
api.add_resource(StoreList, "/stores")
api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")
api.add_resource(UserRegister, "/register")
api.add_resource(User, "/user/<int:user_id>")
api.add_resource(UserLogin, "/login")
api.add_resource(TokenRefresh, "/refresh")
api.add_resource(UserLogout, "/logout")

if __name__ == "__main__":
    db.init_app(app)
    app.run(port=5000, debug=True)

```


> Il y a:
>
> - Importations (jusqu'à la ligne 10)
> - Création et configuration d'applications (jusqu'à la ligne 21)
> - Définission de ce qu'il faut faire avant que la première demande ne frappe notre application (lignes 24-26)
> - Configuration d'erreur d'extension JWT (lignes 29-100)
> - Enregistrement des ressources (lignes 103-111)
> - Que faire lorsque nous exécutons ce fichier (lignes 113-115)


## 1 - Les Importations

Les importations sont assez explicites - nous avons besoin de Flask, Flask-RESTful, Flask-JWT-Extended, notre base de données (Flask-SQLAlchemy), notre liste noire (pour les déconnexions, plus sur cela plus tard) et nos ressources.

## 2 - Création et configuration d'applications

Nous créons ensuite notre application et définissons certains paramètres de configuration. Dans cette application, nous définissons:

> - `SQLALCHEMY_DATABASE_URI`: quelle base de données nous voulons utiliser. Pour l'instant, nous utilisons `SQLite` car il est très facile de travailler avec. Plus tard et lors du déploiement, nous utiliserions `PostgreSQL`.
>
> - `SQLALCHEMY_TRACK_MODIFICATIONS`: une configuration pour Flask-SQLAlchemy qui garde la trace des modifications apportées aux modèles SQLAlchemy. SQLAlchemy le fait déjà aussi, donc souvent nous n'en avons pas besoin. Pour plus d'informations, consultez cette réponse [StackOverflow](https://stackoverflow.com/questions/33738467/how-do-i-know-if-i-can-disable-sqlalchemy-track-modifications/33790196#33790196).
>
> - `PROPAGATE_EXCEPTIONS`: un paramètre de configuration presque entièrement non documenté de Flask, il est nécessaire que les exceptions Flask-RESTful apparaissent dans notre application comme des erreurs au lieu de planter notre serveur avec une "erreur de serveur interne" générique.
>
> - `JWT_BLACKLIST_ENABLED`: activer ou non la liste noire dans Flask-JWT-Extended. Il est nécessaire pour permettre aux utilisateurs de se déconnecter.
>
> - `JWT_BLACKLIST_TOKEN_CHECKS`: quels jetons comparer avec la liste noire. Il y en a deux: les jetons d'accès et les jetons de rafraîchissement.

## 3 - Avant la première demande

Avant la première demande, nous voulons créer toutes nos tables dans la base de données. Cela ne fonctionne que si aucune base de données n'est déjà créée, il est donc très utile pour exécuter notre application localement.

N'oubliez pas de supprimer notre fichier `data.db` (qui est créé lors de notre première demande) chaque fois que vous modifiez l'un des modèles, afin que la base de données soit reconstruite.


## 4 - Configuration Flask-JWT

Essentiellement, il ajoute plus de fonctionnalités à Flask-JWT-Extended, telles que les revendications et la gestion des erreurs.

> Elles sont:
>
> - `@jwt.user_claims_loader`: quelles informations supplémentaires ajouter au JWT (dans notre code initial, nous ajoutons un indicateur pour nous dire si l'utilisateur est un administrateur ou non selon son user `id`).
>
> - `@jwt.token_in_blacklist_loader`: comment vérifier si un jeton est sur liste noire ou non (dans notre code initial, nous comparons l'ID unique du jeton à ce que nous avons stocké dans notre ensemble de liste noire).
>
> - `@jwt.expired_token_loader`: quel message d'erreur renvoyer lorsqu'un jeton a expiré mais que l'utilisateur a quand même essayé de l'utiliser pour accéder à une ressource.
>
> - `@jwt.invalid_token_loader`: quel message d'erreur renvoyer lorsqu'un jeton est utilisé, mais n'est pas valide.
>
> - `@jwt.unauthorized_loader`: quel message d'erreur renvoyer lorsqu'une demande ne contient pas l'autorisation requise (par exemple JWT manquant).
>
> - `@jwt.needs_fresh_token_loader`: quel message d'erreur renvoyer lorsqu'un nouveau jeton est requis mais qu'un jeton non nouveau nous est donné.
>
> - `@jwt.revoked_token_loader`: quel message d'erreur renvoyer lorsqu'un jeton est utilisé mais qu'il a été mis sur liste noire.


## 5 - Enregistrement des ressources

Chaque ressource est une classe qui définit certaines méthodes. Chaque méthode correspond à un verbe HTTP (par exemple `get()` pour une demande `GET`).

L'enregistrement des ressources est l'endroit où nous définissons les URL pour elles:

```python

api.add_resource(Store, "/store/<string:name>")
api.add_resource(StoreList, "/stores")
api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")
api.add_resource(UserRegister, "/register")
api.add_resource(User, "/user/<int:user_id>")
api.add_resource(UserLogin, "/login")
api.add_resource(TokenRefresh, "/refresh")
api.add_resource(UserLogout, "/logout")

```

## 6 - Exécuter l'application

Enfin, nous arrivons à la fin du fichier où nous exécutons l'application.

> **NOTE :**
> En Python, `if __name__ == "__main__"`: est une construction fréquemment utilisée qui signifie que le bloc de code suivant ne s'exécute que si ce fichier a été exécuté. Il ne s'exécute pas si le fichier a été importé.


Ici, nous enregistrons notre base de données avec notre application et commençons à exécuter notre application sur le port 5000 et en mode débogage.


```python

if __name__ == "__main__":
    db.init_app(app)
    app.run(port=5000, debug=True)

```

