# Explication En détail: ressources de l'article

>  
> **NOTE:**
>  
> Les ressources sont des définitions de la façon dont les utilisateurs interagissent avec notre application, nous les définissons donc pour traiter les demandes des utilisateurs. Les ressources utiliseront des modèles pour communiquer avec d'autres parties de l'application et interagir avec la base de données.
>  


Chaque ressource est une classe. Des méthodes spécialement nommées définies à l'intérieur sont utilisées par Flask-RESTful pour répondre à la demande de l'utilisateur. La méthode `get` (qui peut être une méthode d'instance, statique ou de classe) est utilisée pour répondre aux requêtes `GET`. De même , nous pouvons définir d'autres méthodes telles que `post, delete` ou `put`.

> La ressource `Item` a:
>  
>  - `get(name)`: les clients peuvent l'utiliser pour récupérer des informations sur un élément;
>  - `post(name)`: les clients peuvent l'utiliser pour créer un élément;
>  - `put(name)`: les clients peuvent l'utiliser pour mettre à jour un élément, ou le créer s'il n'existe pas; et
>  - `delete(name)`: les clients peuvent l'utiliser pour supprimer un élément.


> Il y a aussi une ressource `ItemList` qui a:
>  - `get()`: les clients peuvent l'utiliser pour récupérer des données sur tous les articles de notre collection

### Code pour `resources/item.py`

```python

from flask_restful import Resource, reqparse
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims,
    get_jwt_identity,
    jwt_optional,
    fresh_jwt_required,
)
from models.item import ItemModel


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        "price", type=float, required=True, help="Cette case ne peut pas être laissée vide!"
    )
    parser.add_argument(
        "store_id", type=int, required=True, help="Chaque élément a besoin d'un store_id."
    )

    @jwt_required
    def get(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            return item.json(), 200
        return {"message": "Objet non-trouvé."}, 404

    @fresh_jwt_required
    def post(self, name):
        if ItemModel.find_by_name(name):
            return (
                {"message": "Un élément avec le nom '{}' existe déjà.".format(name)},
                400,
            )

        data = Item.parser.parse_args()
        item = ItemModel(name, **data)

        try:
            item.save_to_db()
        except:
            return {"message": "Une erreur s'est produite lors de l'insertion de l'élément."}, 500

        return item.json(), 201

    @jwt_required
    def delete(self, name):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Privilège administrateur requis."}, 401

        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
            return {"message": "Élément supprimé."}, 200
        return {"message": "Objet non-trouvé."}, 404

    def put(self, name):
        data = Item.parser.parse_args()

        item = ItemModel.find_by_name(name)

        if item:
            item.price = data["price"]
        else:
            item = ItemModel(name, **data)

        item.save_to_db()

        return item.json(), 200


class ItemList(Resource):
    @jwt_optional
    def get(self):
        user_id = get_jwt_identity()
        items = [item.json() for item in ItemModel.find_all()]
        if user_id:
            return {"items": items}, 200
        return (
            {
                "items": [item["name"] for item in items],
                "message": "Plus de données disponibles si vous vous connectez.",
            },
            200,
        )

```

Outre les définitions de méthode, les ressources contiennent souvent un moyen d'analyser les données entrantes. Lorsque les utilisateurs font des demandes à notre API, ils peuvent envoyer des données dans le corps de la demande. Il s'agit normalement de données de formulaire ou de JSON.

>  
> **NOTE:**
>  
> Consultez la page des fonctionnalités d' authentification JWT des informations sur les décorateurs utilisés dans ce code ( `@jwt_required`, `@fresh_jwt_required` et `@jwt_optional`) et sur `get_jwt_identity()` et `get_jwt_claims()`.
>  

Ce code utilise ici `reqparse` pour définir un objet qui peut comprendre les deux façons de données entrantes. Ensuite, lorsque nous utilisons `parser.parse_args()`, il va dans le corps de la demande et extrait les données.

```python

...
    @fresh_jwt_required
    def post(self, name):
        if ItemModel.find_by_name(name):
            return (
                {"message": "Un élément avec le nom '{}' existe déjà.".format(name)},
                400,
            )

        data = Item.parser.parse_args()
        item = ItemModel(name, **data)

        try:
            item.save_to_db()
        except:
            return {"message": "Une erreur s'est produite lors de l'insertion de l'élément."}, 500

        return item.json(), 201

...

```

## La ressource `Item`

Le chemin pour comprendre ce que fait la ressource `Item` commence `app.py` lorsque la ressource est enregistrée:

```python

api.add_resource(Store, "/store/<string:name>")
api.add_resource(StoreList, "/stores")
api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")
api.add_resource(UserRegister, "/register")

```

Cela définit que l'URL pour accéder à la ressource `Item` est `/item/<string:name>`, ce qui signifie qu'elle pourrait être `/item/chair`, ou toute autre chaîne dans la deuxième partie du chemin.

Les méthodes définies dans la ressource peuvent ensuite utiliser cette chaîne - dans notre cas, nous utilisons cette chaîne comme nom de l'élément (pour récupérer l'élément de la base de données, pour en créer un nouveau ou pour en supprimer un).

La ressource `ItemList` n'a pas un tel paramètre car elle n'a pas besoin du nom d'un élément spécifique, car elle renvoie des informations sur tous les éléments.

### Obtenez un article

Pour obtenir un élément, l'utilisateur doit fournir un JWT valide car la méthode est marquée comme `@jwt_required`:
```python

@jwt_required
def get(self, name):
    item = ItemModel.find_by_name(name)
    if item:
        return item.json(), 200
    return {"message": "Objet non-trouvé."}, 404

```

Ensuite, cette méthode va dans la base de données pour rechercher un élément par le nom fourni dans l'URL et renvoie son dictionnaire JSON s'il est disponible, ou un message dans le cas contraire.
Au lieu de simplement retourner, `item.json()` nous pouvons retourner un tuple à partir de ces méthodes, et Flask-RESTful utilisera le deuxième élément du tuple comme code d'état de la réponse.

Si nous trouvions un article avec succès dans la base de données par exemple, nous retournerions le JSON de l'article avec un code d'état de `200` (ce qui signifie `OK`).

Si nous n'avons pas trouvé l'article, nous renvoyons un message avec un code d'état de `404` (ce qui signifie `non trouvé`).

>  
> **NOTE: Les codes d'etat HTTP**
>  
>  - 200 OK
>  - 201 CREATED
>  - 301 PERMANENT REDIRECT
>  - 400 BAD REQUEST
>  - 401 UNAUTHORIZED
>  - 404 NOT FOUND
>  - 500 INTERNAL SERVER ERROR
>  

### Créer un article

La création d'un élément nécessite un nouveau JWT car, par exemple, nous avons décidé de traiter ce point de terminaison comme un point de terminaison "très sensible":

```python

 @fresh_jwt_required
    def post(self, name):
        if ItemModel.find_by_name(name):
            return (
                {"message": "Un élément avec le nom '{}' existe déjà.".format(name)},
                400,
            )

        data = Item.parser.parse_args()
        item = ItemModel(name, **data)

        try:
            item.save_to_db()
        except:
            return {"message": "Une erreur s'est produite lors de l'insertion de l'élément."}, 500

        return item.json(), 201

```

En termes généraux, cela signifie qu'il doit s'agir du premier JWT demandé par l'utilisateur depuis la connexion et ne peut pas être un JWT généré via la fonctionnalité de rafraîchissement de jeton. Cela garantit que l'utilisateur vient de saisir son mot de passe.

Un nouveau jeton est le jeton le plus sécurisé car il signifie que l'utilisateur vient de s'authentifier.

> Cette méthode ensuite:
>   1 - Recherche un élément dans la base de données;<br>
>     1 - S'il existe, renvoyez un message d'erreur.<br>
>   2 - Sinon, analysez les arguments qui se trouvent dans le corps de la demande (tels que le `price` et `store_id`);<br>
>   3 - Créez l'élément et enregistrez-le dans la base de données;<br>
>     1 - Renvoie un message d'erreur en cas d'échec.<br>
>   4 - Renvoyez le JSON de l'élément nouvellement créé, avec un code de statut 201 (ce qui signifie CREATED)

### Supprimer un élément

Pour supprimer un élément, nous avons également besoin d'un JWT. Cependant, l'JWT doit avoir une allégation selon laquelle il `is_admin` est ` True `.

Si la réclamation n'est pas présente ou n'existe pas `True`, nous renvoyons un message d'erreur et un code d'état 401, ce qui signifie `UNAUTHORIZED`.

```python
    @jwt_required
    def delete(self, name):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Privilège administrateur requis."}, 401

        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
            return {"message": "Élément supprimé."}, 200
        return {"message": "Objet non-trouvé."}, 404

```

Sinon, nous allons de l'avant et trouvons l'élément et le supprimons, en renvoyant les messages et les codes d'état appropriés.

### Mettre à jour un élément

La mise à jour d'un élément est la seule méthode de la ressource qui peut faire deux choses. Il peut soit créer un nouvel élément, soit le mettre à jour s'il existe déjà.

> **NOTE :**
>   ` PUT ` les demandes sont censées être **idempotentes** , ce qui signifie que vous pouvez répéter une demande plusieurs fois et le serveur se retrouvera toujours avec les mêmes données, quel que soit le nombre de fois que vous l'exécutez.
>   Par exemple, si vous effectuez la même demande ` POST `  plusieurs fois, la deuxième fois, vous obtiendrez une erreur indiquant que l'élément existe déjà. Si vous effectuez ` PUT ` plusieurs fois la même demande, la première créera un nouvel objet et les suivants le mettront à jour pour avoir les mêmes données qu'il possède déjà. Le serveur se retrouvera donc avec les mêmes données quel que soit le nombre de fois que vous exécutez le demande.

Le code de cette demande est le suivant:

```python
def put(self, name):
    data = Item.parser.parse_args()

    item = ItemModel.find_by_name(name)

    if item:
        item.price = data["price"]
    else:
        item = ItemModel(name, **data)

    item.save_to_db()

    return item.json(), 200

```

1 - Récupérer les données du corps de la demande à l'aide de l'analyseur;<br>
2 - Trouvez l'article par son nom;<br>
3 - S'il existe, mettez à jour son prix (c'est la seule chose que nous autorisons les mises à jour);<br>
4 - Sinon, créez un nouvel élément;<br>
5 - Enregistrez-le dans la base de données;<br>
6 - Renvoyez le JSON de l'article.<br>

## La ressource `ItemList`

La ressource `ItemList` utilise un décorateur `@jwt_optional` qui peut être très pratique si vous voulez que les utilisateurs déconnectés puissent voir certaines données, mais permettre aux utilisateurs connectés de voir plus de données.

Dans la méthode `get()` de cette ressource, nous récupérons une liste de tous les articles dans tous nos magasins.

Si l'utilisateur est déconnecté, il ne voit que les noms des éléments. Si, ils sont enregistrés qu'ils obtiennent pour voir l'article complet JSON ( `id`, `name`, `price` et `store_id`):

```python

class ItemList(Resource):
    @jwt_optional
    def get(self):
        user_id = get_jwt_identity()
        items = [item.json() for item in ItemModel.find_all()]
        if user_id:
            return {"items": items}, 200
        return (
            {
                "items": [item["name"] for item in items],
                "message": "Plus de données disponibles si vous vous connectez.",
            },
            200,
        )

```

Pour ce faire, nous utilisons ` get_jwt_identity() `. 
Cela nous donne un ID utilisateur si nous avons reçu un JWT dans la demande. Si nous n'avons pas reçu de JWT, nous renvoyons ` None ` - ce qui signifie que l'utilisateur n'est pas connecté.

Ensuite, si nous en avons un ` user_id `, nous retournons la charge utile complète de l'article. Sinon, nous renvoyons simplement les noms des éléments et un message indiquant qu'il y a plus d'informations disponibles pour les utilisateurs connectés.

Il s'agit d'un modèle très populaire et très facile à implémenter à l'aide de Flask-JWT-Extended.


# Explication En détail: les ressources stocker

> La ressource ` Store ` a:

>    - ` get(name) `: les clients peuvent l'utiliser pour récupérer des informations sur un magasin;
>    - ` post(name) `: les clients peuvent l'utiliser pour créer un magasin;
>    - ` delete(name) `: les clients peuvent l'utiliser pour supprimer un magasin.

> Il y a aussi une ressource ` StoreList ` qui a:

>    - ` get() `: les clients peuvent l'utiliser pour récupérer des données sur tous les magasins de notre collection et leurs articles.


### Code pour resources/store.py


```python

from flask_restful import Resource
from models.store import StoreModel


class Store(Resource):
    def get(self, name):
        store = StoreModel.find_by_name(name)
        if store:
            return store.json()
        return {"message": "Magasin introuvable."}, 404

    def post(self, name):
        if StoreModel.find_by_name(name):
            return (
                {"message": "Un magasin avec le nom '{}' existe déjà.".format(name)},
                400,
            )

        store = StoreModel(name)
        try:
            store.save_to_db()
        except:
            return {"message": "Une erreur s'est produite lors de la création du magasin."}, 500

        return store.json(), 201

    def delete(self, name):
        store = StoreModel.find_by_name(name)
        if store:
            store.delete_from_db()

        return {"message": "Magasin supprimé."}


class StoreList(Resource):
    def get(self):
        return {"stores": [x.json() for x in StoreModel.find_all()]}

```

Maintenant que vous avez pris connaissance de la ressource `Item`, sa compréhension `Store` est très simple, car:

> - Il est beaucoup plus simple ressource sans argument et l'analyse syntaxique uniquement à l' aide du paramètre d'URL, `name`.
> - Il n'utilise pas Flask-JWT-Extended, il n'y a donc pas d'exigences JWT.
> - Il y a très peu de logique, c'est principalement une ressource qui accepte et récupère les données de la base de données à l'aide de modèles.

## Création d'un magasin avant un article

Une question importante est: **devez-vous créer un magasin avant de créer votre premier article** ?

Un article a besoin d'une propriété `store_id`, ce qui signifie que s'il n'y a pas de magasins avant de créer un article, vous ne devriez pas pouvoir attribuer une valeur aux articles `store_id` (car il n'y a pas de magasins avec un `id`).

Si vous exécutez cette application à l'aide d'une base de données à part entière, telle que `PostgreSQL` ou `MySQL`, vous constaterez que vous devez créer un magasin avant de pouvoir créer un élément, de sorte que le `id` du magasin et le `store_id` de l'article correspondent.

Cependant, si vous l'exécutez à l'aide de `SQLite` (comme nous le faisons dans nos tests), cette restriction ne s'applique pas. Vous pourriez donner à un article un magasin `store_id` qui n'existe pas et `SQLite` l'accepterait. C'est juste une de ces choses à propos de l'utilisation d'une base de données SQL "lite".

> **ATTENTION**
>  SQLite n'applique pas les clés étrangères, telles que `store_id`, pour avoir un équivalent de clé primaire valide. Cela vaut la peine de se rappeler si vous testez votre application avec un système de base de données différent de celui qui s'exécutera en production (ce qui n'est souvent pas recommandé!).
>
>  Pour une confiance maximale dans votre application, exécutez-la au moins en utilisant la même base de données que vous utiliserez en production avant le déploiement. Même si vous développez à l'aide de SQLite, assurez-vous de faire un essai sur votre système de base de données de production avant de le mettre en ligne!

# Explication En détail: Ressources utilisateur

Il existe plusieurs ressources utilisateur, chacune ne contenant qu'une ou deux méthodes.

La plupart de ces ressources répondent aux demandes `POST` car c'est la méthode utilisée pour répondre à des données arbitraires, et pas nécessairement créer des modèles et les enregistrer sur notre serveur (par exemple, lorsque l'utilisateur nous envoie des données pour la connexion, rien n'est créé).

### L'analyseur utilisateur

Cet analyseur est défini en dehors d'une ressource spécifique, car il est utilisé par plusieurs ressources. Vous n'avez pas besoin de définir l'analyseur dans une ressource.

Cet analyseur recherchera deux champs dans le corps de la demande (rappelez-vous que ce sont les données du formulaire ou la charge utile JSON): `username` et `password`.


```python

_user_parser = reqparse.RequestParser()
_user_parser.add_argument(
    "username", type=str, required=True, help="Ce champ ne peut être vide."
)
_user_parser.add_argument(
    "password", type=str, required=True, help="Ce champ ne peut être vide."
)

```

#### `UserRegister`

```python

class UserRegister(Resource):
    def post(self):
        data = _user_parser.parse_args()

        if UserModel.find_by_username(data["username"]):
            return {"message": "un utilisateur avec ce pseudo existe déjà."}, 400

        user = UserModel(**data)
        user.save_to_db()

        return {"message": "L'utilisateur a été créé avec succès."}, 201

```

Cette ressource examine les données transmises par la demande et en crée une nouvelle, `UserModel` sauf si un utilisateur existe déjà avec ce nom.

> **ATTENTION**

>  Notez que les mots de passe ne sont pas chiffrés dans cet exemple d'application. Si vous deviez mettre cela en production, vous voudriez crypter les mots de passe avant de les enregistrer dans la base de données.

>  En utilisant Python et la bibliothèque populaire `passlib`, ce ne sont que quelques lignes de code supplémentaires.  [façon de mettre en œuvre le cryptage des mots de passe dans ce type d'application](https://blog.tecladocode.com/learn-python-encrypting-passwords-python-flask-and-passlib/).

#### `User`

Cette ressource est utilisée uniquement pour les tests, pour récupérer et supprimer des utilisateurs existants. Avant de mettre votre application en production, vous devez supprimer ce point de terminaison.

Nous l'avons ajouté afin que vous n'ayez pas à supprimer votre base de données (ou à trouver des noms d'utilisateurs de plus en plus aléatoires) lorsque vous développez votre API.

```python

class User(Resource):
    @classmethod
    def get(cls, user_id: int):
        user = UserModel.find_by_id(user_id)
        if not user:
            return {"message": "Utilisateur non trouvé."}, 404
        return user.json(), 200

    @classmethod
    def delete(cls, user_id: int):
        user = UserModel.find_by_id(user_id)
        if not user:
            return {"message": "Utilisateur non trouvé."}, 404
        user.delete_from_db()
        return {"message": "L'utilisateur a été supprimé."}, 200

```

#### `UserLogin`

Cette ressource utilise Flask-JWT-Extended pour générer un JWT (également appelé jeton d'accès) et un jeton d'actualisation pour un utilisateur donné.

Les JWT contiennent des charges utiles codées (dans notre cas, le champ id de l'utilisateur ) que nous pouvons ensuite utiliser pour récupérer à quel utilisateur le JWT est destiné lorsqu'il revient dans une autre demande.

Cette ressource vérifie la combinaison nom d'utilisateur/mot de passe pour s'assurer qu'elle est correcte.

```python

class UserLogin(Resource):
    def post(self):
        data = _user_parser.parse_args()

        user = UserModel.find_by_username(data["username"])

        if user and safe_str_cmp(user.password, data["password"]):
            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)
            return {"access_token": access_token, "refresh_token": refresh_token}, 200

        return {"message": "Les informations d'identification sont invalides!"}, 401

```

Notez que le jeton d'accès retourné par cette méthode est créé avec la fonction `create_access_token`, avec le paramètre `fresh=True`. Étant donné que ce point de terminaison répond directement à une connexion utilisateur, le jeton d'accès renvoyé par lui sera toujours actualisé.

> **NOTES :**
>
> Utilisez Werkzeug `safe_str_cmp` pour comparer des chaînes lorsque vous ne contrôlez pas les entrées (par exemple, l'entrée utilisateur, comme cela est fait ici).
>
> Il s'assure que ce sont des chaînes et fonctionne avec toutes les versions de Python - vous n'avez donc pas à vous soucier des chaînes d'octets embêtants ou quelque chose comme ça!

#### `UserLogout`

Cette ressource utilise notre liste noire. Lorsqu'un utilisateur fait une demande à cette ressource, nous enregistrons l'identifiant unique de son jeton d'accès (différent de son utilisateur `id`!) Dans la liste noire, afin que ce jeton d'accès spécifique ne puisse pas être réutilisé.

S'ils veulent se reconnecter, ils peuvent le faire et un nouveau jeton d'accès sera généré.

```python

class UserLogout(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()["jti"]  # jti is "JWT ID", a unique identifier for a JWT.
        user_id = get_jwt_identity()
        BLACKLIST.add(jti)
        return {"message": "Déconnexion réussie pour l'utilisateur <id={}>.".format(user_id)}, 200

```

Cela utilise la fonction `get_raw_jwt()` de Flask-JWT-Extended. Il nous donne un dictionnaire des différentes propriétés stockées à l'intérieur du JWT décodé.


#### `TokenRefresh`

Cette ressource prend un jeton d'actualisation et nous donne un jeton d'accès non actualisé.

Les jetons d'accès expirent généralement environ 10 minutes après leur génération (certains durent plus longtemps, parfois jusqu'à quelques jours). L'actualisation des jetons permet aux applications qui utilisent votre API de continuer à demander de nouveaux jetons d'accès sans que l'utilisateur ait besoin de se connecter encore et encore.

Cependant, cela nous donne des **jetons non active**. Si nous avons une opération très sensible et que nous voulons vérifier que l'utilisateur est bien lui, nous avons besoin qu'il se reconnecte. Si cela se produit, nous pouvons marquer n'importe quel point final comme nécessitant **un nouveau jeton d'accès**.

```python

class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {"access_token": new_token}, 200

```

