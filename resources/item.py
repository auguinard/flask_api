from flask_restful import Resource, reqparse
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims,
    get_jwt_identity,
    jwt_optional,
    fresh_jwt_required,
)
from models.item import ItemModel


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        "price", type=float, required=True, help="Cette case ne peut pas être laissée vide!"
    )
    parser.add_argument(
        "store_id", type=int, required=True, help="Le Chaque élément a besoin d'un store_id."
    )

    @jwt_required  # Plus besoin de supports
    def get(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            return item.json(), 200
        return {"message": "Objet non-trouvé."}, 404

    @fresh_jwt_required
    def post(self, name):
        if ItemModel.find_by_name(name):
            return (
                {"message": "Un élément portant le nom '{}' existe déjà.".format(name)},
                400,
            )

        data = Item.parser.parse_args()

        item = ItemModel(name, **data)

        try:
            item.save_to_db()
        except:
            return {"message": "Une erreur s'est produite lors de l'insertion de l'élément."}, 500

        return item.json(), 201

    @jwt_required
    def delete(self, name):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Privilège administrateur requis."}, 401

        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
            return {"message": "Élément supprimé."}, 200
        return {"message": "Objet non-trouvé."}, 404

    def put(self, name):
        data = Item.parser.parse_args()

        item = ItemModel.find_by_name(name)

        if item:
            item.price = data["price"]
        else:
            item = ItemModel(name, **data)

        item.save_to_db()

        return item.json(), 200


class ItemList(Resource):
    @jwt_optional
    def get(self):
        """
        Ici, nous obtenons l'identité JWT, puis si l'utilisateur est connecté (nous avons pu obtenir une identité)
         nous renvoyons la liste complète des articles.

         Sinon, nous renvoyons simplement les noms des articles.

         Cela pourrait être fait avec par exemple voir les commandes qui ont été passées, mais ne pas voir les détails des commandes
         sauf si l'utilisateur s'est connecté.
        """
        user_id = get_jwt_identity()
        items = [item.json() for item in ItemModel.find_all()]
        if user_id:
            return {"items": items}, 200
        return (
            {
                "items": [item["name"] for item in items],
                "message": "Plus de données disponibles si vous vous connectez.",
            },
            200,
        )
