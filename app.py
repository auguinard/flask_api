from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager

from db import db
from blacklist import BLACKLIST
from resources.user import UserRegister, UserLogin, User, TokenRefresh, UserLogout
from resources.item import Item, ItemList
from resources.store import Store, StoreList

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["PROPAGATE_EXCEPTIONS"] = True
app.config["JWT_BLACKLIST_ENABLED"] = True  # activer la fonction de liste noire
app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = [
    "access",
    "refresh",
]  # autoriser la mise sur liste noire pour l'accès et actualiser les jetons
app.secret_key = "jose"  # app.config ['JWT_SECRET_KEY']
api = Api(app)


@app.before_first_request
def create_tables():
    db.create_all()


jwt = JWTManager(app)


@jwt.user_claims_loader
def add_claims_to_jwt(
    identity
):  # N'oubliez pas que l'identité est ce que nous définissons lors de la création du jeton d'accès
    if (
        identity == 1
    ):  # au lieu de coder en dur, nous devrions lire à partir d'un fichier ou d'une base de données pour obtenir une liste d'administrateurs à la place
        return {"is_admin": True}
    return {"is_admin": False}


# Cette méthode vérifiera si un jeton est sur liste noire et sera appelée automatiquement lorsque la liste noire est activée
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    return (
        decrypted_token["jti"] in BLACKLIST
    )  # Nous listons ici les JWT particuliers qui ont été créés dans le passé.


# Les rappels suivants sont utilisés pour personnaliser les messages de réponse / d'erreur jwt.
# Les originaux peuvent ne pas être dans un très joli format
@jwt.expired_token_loader
def expired_token_callback():
    return jsonify({"message": "The token has expired.", "error": "token_expired"}), 401


@jwt.invalid_token_loader
def invalid_token_callback(
    error
):  # nous devons garder l'argument ici, car il est transmis par l'appelant en interne
    return (
        jsonify(
            {"message": "Vérification de signature a échoué.", "error": "invalid_token"}
        ),
        401,
    )


@jwt.unauthorized_loader
def missing_token_callback(error):
    return (
        jsonify(
            {
                "description": "La demande ne contient pas de jeton d'accès.",
                "error": "authorization_required",
            }
        ),
        401,
    )


@jwt.needs_fresh_token_loader
def token_not_fresh_callback():
    return (
        jsonify(
            {"description": "Le jeton n'est pas frais.", "error": "fresh_token_required"}
        ),
        401,
    )


@jwt.revoked_token_loader
def revoked_token_callback():
    return (
        jsonify(
            {"description": "Le jeton a été révoqué.", "error": "token_revoked"}
        ),
        401,
    )


api.add_resource(Store, "/store/<string:name>")
api.add_resource(StoreList, "/stores")
api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")
api.add_resource(UserRegister, "/register")
api.add_resource(User, "/user/<int:user_id>")
api.add_resource(UserLogin, "/login")
api.add_resource(TokenRefresh, "/refresh")
api.add_resource(UserLogout, "/logout")

if __name__ == "__main__":
    db.init_app(app)
    app.run(port=5000, debug=True)
