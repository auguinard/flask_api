# Explications En détail: modèle d'article

> **NOTE :**
>         Un modèle est une représentation de ce que notre application traite en interne. Chaque fois que deux parties de notre application communiquent entre elles, elles ces communications se feront principalement en utilisant des modèles.

### Code pour models/item.py

```python

from db import db


class ItemModel(db.Model):
    __tablename__ = "items"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(precision=2))

    store_id = db.Column(db.Integer, db.ForeignKey("stores.id"))
    store = db.relationship("StoreModel")

    def __init__(self, name, price, store_id):
        self.name = name
        self.price = price
        self.store_id = store_id

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "price": self.price,
            "store_id": self.store_id,
        }

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

```

Les modèles de notre application peuvent se résumer à trois choses:

> - Ils stockent certaines données à utiliser dans notre application. C'est ce qui est défini dans la méthode `__init__`, dans le cas `ItemModel` qui est le `name, price` et `store_id`.

> - Ils définissent les colonnes auxquelles ils correspondent dans notre base de données. Souvent, cela inclut tous les paramètres de la méthode `__init__`, mais peut également en inclure plus (comme la colonne `id`, qui est auto-incrémentée et générée par notre base de données).

> - Il contient quelques méthodes qui nous permettent d'interagir plus facilement avec la base de données.

## 1 - ItemModel

### Recherche d'un article


La recherche d'un élément est facilitée car il s'agit d'un  SQLAlchemy `Model`: `ItemModel`, nous pouvons donc utiliser `Item.query` pour interagir avec la table des éléments de notre base de données.
Par exemple, pour trouver un élément spécifique, nous définissons la méthode `find_by_name`  dans le modèle, ce qui fait ceci:

```python

@classmethod
def find_by_name(cls, name: str) -> "ItemModel":
    return cls.query.filter_by(name=name).first()

```

À partir de toute autre partie de notre application, nous pouvons ensuite trouver un élément dans la base de données avec cette méthode:

```python

ItemModel.find_by_name("chair")

```

Et nous récupérons un élément dont la colonne de nom est "chaise".

### Récupération de tous les éléments de la base de données

Pour récupérer tous les éléments de la base de données, nous pouvons utiliser:

```python

ItemModel.find_all()

```

### Enregistrement d'un élément dans la base de données

Pour enregistrer un élément dans la base de données, nous devons d'abord créer un objet `ItemModel`, puis l'enregistrer dans la session de base de données en cours:

```python

my_item = ItemModel(name="Chair", price=15.99, store_id=1)
db.session.add(my_item)
db.session.commit()

```
Mais comme ItemModel a déjà une méthode qui s'ajoute à la session et valide la connexion, nous pouvons l'utiliser:

```python

my_item = ItemModel(name="Chair", price=15.99, store_id=1)
my_item.save_to_db()

```

### Suppression d'un élément de la base de données

De manière similaire à l'ajout d'un élément à la base de données, nous pouvons supprimer un élément de la session de base de données, puis valider la connexion:

```python

my_item = ItemModel.find_by_name("Chair")
my_item.delete_from_db()

```

### Retourner un article à notre client via l'API

Normalement, les API REST reçoivent des données et répondent avec des données au format JSON. 
Notre API n'est pas différente, donc lorsque nous voulons donner à un client des données sur un article, nous devons d'abord le convertir en JSON.

Parce que JSON est tellement similaire aux dictionnaires Python, Flask-RESTful se convertira automatiquement d'un dictionnaire Python en JSON pour nous s'il le peut. Tout ce que nous avons à faire pour répondre avec JSON est de donner à Flask-RESTful un dictionnaire avec les données que nous voulons renvoyer.

C'est ce que fait la méthode `ItemModel.json`: transformer les données stockées dans l'objet en un dictionnaire Python.

```python

my_item = ItemModel.find_by_name("Chair")
return my_item.json()

```

# Explication En détail: modèle de magasin

### Code pour `models/store.py`


```python

from db import db


class StoreModel(db.Model):
    __tablename__ = "stores"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))

    items = db.relationship("ItemModel", lazy="dynamic")

    def __init__(self, name):
        self.name = name

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "items": [item.json() for item in self.items.all()],
        }

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

```

## 1 - Relations SQLAlchemy

Ce modèle est très similaire au `ItemModel`, mais nous voyons un nouveau concept près des définitions de colonne:

```python

items = db.relationship("ItemModel", lazy="dynamic")

```

SQLAlchemy est suffisamment intelligent pour pouvoir construire une relation plusieurs-à-un à partir de cela. Chaque élément a une propriété `store_id`, donc la propriété `items` de `StoreModel` devient une liste de ces éléments.

Ou du moins, ce serait le cas si nous supprimions `lazy="dynamic"`.

Avec `lazy="dynamic"`, `items` devient une requête SQLAlchemy, donc chaque fois que nous voulons accéder aux articles du magasin, nous devons faire quelque chose comme ceci:

```python

store = StoreModel.find_by_name("Amazon")
print(store.items.all())

```

`.all()` C'est l'élément clé ici, et seulement nécessaire quand `lazy="dynamic"`. Puisqu'il s'agit d'une requête SQLAlchemy `store.items`,la `.all()` fait entrer dans la base de données et récupérer tous les éléments. Nous appelons cela une propriété "paresseuse" car les éléments ne sont pas chargés avant que nous fassions `.all()`.

Si nous retirons `lazy="dynamic"`, les éléments sont chargés à partir de la base de données dès que l'objet `StoreModel` est créé.

Cela peut être assez puissant, car si nous ajoutons un nouvel élément après la création de l'objet `StoreModel`, nous pouvons toujours voir que l'élément est lié au magasin lors du chargement à partir de la base de données. Regardons un exemple:

### Ajout d'un article à un magasin

Chaque fois qu'un élément est créé et enregistré dans la base de données avec le bon `store_id`, SQLAlchemy peut le relier au magasin. Par exemple:

```python

store = StoreModel("Amazon")
store.save_to_db()  # Cela donne à notre magasin un 'id' car il est généré par la base de données

item = ItemModel("Chair", 12.99, 1)
item.save_to_db()

print(store.items.all())  # Nous donnera ce président

```

### Exemple sans: `lazy="dynamic"`

Imaginez que nous avons supprimé `lazy="dynamic"` de la définition `db.relationship`.
Le code ci-dessus se changerait en ceci (supprimer `.all()`):

```python

store = StoreModel("Amazon")
store.save_to_db()  # Cela donne à notre magasin un 'id' car il est généré par la base de données

item = ItemModel("Chair", 12.99, 1)
item.save_to_db()

print(store.items)  # Nous donnera une liste vide

```

Et le code ne nous donnerait plus le nouvel élément, car il a été créé après la création de l'objet `StoreModel`.


# Explication En détail: modèle utilisateur

Le UserModel est très similaire au ItemModel car il ne fait rien par lui-même.
C'est juste un conteneur de données, avec quelques méthodes d'assistance pour nous faciliter la vie.

> **ATENTION:**
> Notez que la méthode `.json()` ne renvoie pas de dictionnaire contenant le mot de passe, car nous voulons garder le mot de passe privé et ne jamais l'exposer dans nos réponses de point de terminaison API.

### Code pour models/user.py

```python

from db import db


class UserModel(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def json(self):
        return {"id": self.id, "username": self.username}

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

```


